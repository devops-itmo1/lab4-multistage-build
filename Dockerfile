FROM golang:alpine AS build

# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git

WORKDIR /app

COPY . .

# Build the binary.
RUN go build ./cmd/api/main.go

FROM scratch

COPY --from=build /app/internal/config/.env  /internal/config/.env

# Copy our static executable.
COPY --from=build /app/main /app/main

# Run the hello binary.
ENTRYPOINT ["/app/main"]